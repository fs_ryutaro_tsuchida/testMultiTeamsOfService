//
//  testMultiTermOfServiceApp.swift
//  testMultiTermOfService
//
//  Created by 土田 隆太郎 on 2022/06/18.
//

import SwiftUI

@main
struct testMultiTermOfServiceApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
