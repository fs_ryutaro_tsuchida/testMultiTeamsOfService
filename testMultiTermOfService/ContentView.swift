//
//  ContentView.swift
//  testMultiTermOfService
//
//  Created by 土田 隆太郎 on 2022/06/18.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
